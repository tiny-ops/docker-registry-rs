# Development

## Run tests

Prepare authentication for docker registry:

```bash
# Ubuntu/Debian
sudo apt-get install apache2-utils

# MacOS
brew install httpd

# Create user `test` with password `test`
mkdir auth
htpasswd -Bbn test test > auth/htpasswd

mkdir config
```

Create `config/config.yml` file:

```yaml
version: 0.1

log:
  level: debug
  fields:
    service: registry
storage:
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
  auth:
    htpasswd:
      realm: basic-realm
      path: /auth/htpasswd
```

Run registry:

```bash
docker run --rm -d --name registry -p 5123:5000 \
  -v $(pwd)/auth:/auth -v $(pwd)/config:/etc/docker/registry -v registry_data:/var/lib/registry \
  -e REGISTRY_AUTH=htpasswd -e REGISTRY_AUTH_HTPASSWD_REALM="basic-realm" \
  -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd -e REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY=/var/lib/registry \
  registry:2

docker pull nginx:latest
docker tag nginx:latest localhost:5123/nginx
docker push localhost:5123/nginx
```

Run tests:

```bash
cargo test
```
