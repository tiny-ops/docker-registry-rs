# Docker Registry

Partial implementation just for need of my projects.

Work with [Docker Registry API v2](https://docs.docker.com/registry/spec/api/).

## Getting started

Add dependency:

```toml
docker-registry-rs = { git = "https://gitlab.com/tiny-ops/docker-registry-rs", version = "0.3.0" }
```

Use:

```rust
let client = Client::new();

let service = DockerRegistryServiceImpl::new(&client);

let images = service.get_registry_images("registry.company.com", "my-user", "str0nG-PassW0rd", false);

// Call for insecure registry (via http)
// let images = service.get_registry_images("registry.company.com", "my-user", "str0nG-PassW0rd", true);

println!("{:?}", images);
```

Return vec with `DockerImage`:

```rust
pub struct DockerImage {
    pub name: String,
    pub registry: String,
    pub tag: String,
}
```

### Utils

```toml
docker-registry-rs = { git = "https://gitlab.com/tiny-ops/docker-registry-rs", version = "0.3.0", features = ["utils"] }
```

Adds method `get_random_docker_image()`.

### Mock

Mock implementations `MockSuccessDockerRegistryService` and `MockErrorDockerRegistryService` available with `mock` feature:

```toml
docker-registry-rs = { git = "https://gitlab.com/tiny-ops/docker-registry-rs", version = "0.3.0", features = ["mock"] }
```

## Development

See [DEV.md](DEV.md).

## Troubleshooting

### Get repositories:

```shell
curl https://registry.company.com/v2/_catalog
```

### Get repository tags:

```shell
curl https://registry.company.com/v2/[repo-name]/tags/list
```

Replace `repo-name` with repository name.
