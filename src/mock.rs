use crate::{error::DockerRegistryError, image::DockerImage, service::DockerRegistryService};

/// Mock implementation for `DockerRegistryService` trait
/// Always return `Vec<DockerImage>`
#[derive(Clone, Debug)]
pub struct MockSuccessDockerRegistryService {
    pub get_registry_images_result: Vec<DockerImage>,
}

impl DockerRegistryService for MockSuccessDockerRegistryService {
    async fn get_registry_images(
        &self,
        _registry_host: &str,
        _username: &str,
        _password: &str,
        _insecure: bool,
    ) -> Result<Vec<DockerImage>, DockerRegistryError> {
        Ok(self.get_registry_images_result.clone())
    }
}

#[derive(Clone, Debug)]
pub struct MockErrorDockerRegistryService;

impl DockerRegistryService for MockErrorDockerRegistryService {
    async fn get_registry_images(
        &self,
        _registry_host: &str,
        _username: &str,
        _password: &str,
        _insecure: bool,
    ) -> Result<Vec<DockerImage>, DockerRegistryError> {
        Err(DockerRegistryError::Error)
    }
}
