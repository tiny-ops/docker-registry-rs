use std::future::Future;

use log::{debug, info};
use reqwest::Client;
use serde::Deserialize;

use crate::{error::DockerRegistryError, image::DockerImage};

pub trait DockerRegistryService: Clone + Send + Sync + 'static {
    /// - `registry_host` - for example registry.company.com
    fn get_registry_images(
        &self,
        registry_host: &str,
        username: &str,
        password: &str,
        insecure: bool,
    ) -> impl Future<Output = Result<Vec<DockerImage>, DockerRegistryError>> + Send;
}

const REGISTRY_API_IMAGES_ENDPOINT: &str = "/v2/_catalog";

#[derive(Clone, Debug)]
pub struct DockerRegistryServiceImpl(Client);

impl DockerRegistryServiceImpl {
    pub fn new(client: &Client) -> Self {
        Self(client.clone())
    }

    async fn get_repository_image_tags(
        &self,
        protocol: &str,
        repository_host: &str,
        repository_name: &str,
        username: &str,
        password: &str,
    ) -> Result<Vec<String>, DockerRegistryError> {
        info!("get repository '{}' image tags..", repository_name);

        let url = format!("{protocol}://{repository_host}/v2/{repository_name}/tags/list");

        let image_tags = &self
            .0
            .get(&url)
            .basic_auth(&username, Some(&password))
            .send()
            .await?
            .json::<ImageTagListResponse>()
            .await?;

        debug!("images received:");
        debug!("{:?}", image_tags.tags);

        Ok(image_tags.tags.clone())
    }
}

impl DockerRegistryService for DockerRegistryServiceImpl {
    async fn get_registry_images(
        &self,
        registry_host: &str,
        username: &str,
        password: &str,
        insecure: bool,
    ) -> Result<Vec<DockerImage>, DockerRegistryError> {
        info!("getting registry images from '{registry_host}'..");
        debug!("username '{username}'");

        let mut protocol = "https";

        if insecure {
            protocol = "http";
        }

        let url = format!("{protocol}://{registry_host}{REGISTRY_API_IMAGES_ENDPOINT}");
        debug!("url: '{}'", url);

        let repositories = &self
            .0
            .get(&url)
            .basic_auth(&username, Some(&password))
            .send()
            .await?
            .json::<RepositoryListResponse>()
            .await?;

        debug!("repositories:");
        debug!("{:?}", repositories);

        let mut docker_images: Vec<DockerImage> = vec![];

        for repository_name in &repositories.repositories {
            let image_tags = self
                .get_repository_image_tags(
                    &protocol,
                    &registry_host,
                    &repository_name,
                    &username,
                    &password,
                )
                .await?;

            for image_tag in image_tags {
                docker_images.push(DockerImage {
                    name: repository_name.to_string(),
                    registry: registry_host.to_string(),
                    tag: image_tag,
                })
            }
        }

        Ok(docker_images)
    }
}

#[derive(Deserialize, Debug)]
struct RepositoryListResponse {
    pub repositories: Vec<String>,
}

#[derive(Deserialize, Debug)]
struct ImageTagListResponse {
    pub tags: Vec<String>,
}

#[cfg(test)]
mod get_registry_images_tests {
    use reqwest::Client;

    use log::LevelFilter;

    use crate::service::{DockerRegistryService, DockerRegistryServiceImpl};

    pub fn init_logging() {
        let _ = env_logger::builder()
            .filter_level(LevelFilter::Debug)
            .is_test(true)
            .try_init();
    }

    fn get_registry_host() -> String {
        "localhost:5123".to_string()
    }

    fn get_registry_username() -> String {
        "test".to_string()
    }

    fn get_registry_password() -> String {
        "test".to_string()
    }

    #[tokio::test]
    async fn return_images() {
        init_logging();

        let registry_host = get_registry_host();
        let username = get_registry_username();
        let password = get_registry_password();

        let client = Client::new();

        let service = DockerRegistryServiceImpl::new(&client);

        let images = service
            .get_registry_images(&registry_host, &username, &password, true)
            .await
            .unwrap();

        assert!(images.len() > 0);
    }

    #[tokio::test]
    async fn return_general_error_for_invalid_response() {
        init_logging();

        let registry_host = "https://google.com";
        let username = get_registry_username();
        let password = get_registry_password();

        let client = Client::new();

        let service = DockerRegistryServiceImpl::new(&client);

        match service
            .get_registry_images(&registry_host, &username, &password, true)
            .await
        {
            Ok(_) => panic!("error expected"),
            Err(e) => match e {
                NetworkError => assert!(true),
            },
        }
    }

    #[tokio::test]
    async fn return_general_error_for_invalid_base_url() {
        let registry_host = "invalid-registry-host";
        let username = get_registry_username();
        let password = get_registry_password();

        let client = Client::new();

        let service = DockerRegistryServiceImpl::new(&client);

        assert!(service
            .get_registry_images(&registry_host, &username, &password, true)
            .await
            .is_err());
    }
}
