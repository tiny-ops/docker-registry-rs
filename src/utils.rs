use fake::{Fake, Faker};

use crate::image::DockerImage;

pub fn get_random_docker_image() -> DockerImage {
    DockerImage {
        name: Faker.fake::<String>(),
        registry: Faker.fake::<String>(),
        tag: Faker.fake::<String>(),
    }
}
