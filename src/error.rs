use thiserror::Error;

#[derive(Error, Debug)]
pub enum DockerRegistryError {
    #[error("Network Error")]
    NetworkError(#[from] reqwest::Error),

    #[error("Malformed URL Error")]
    MalformedUrlError,

    #[error("Docker Registry API communication error")]
    Error,
}
