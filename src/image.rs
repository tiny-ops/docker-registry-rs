use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Ord, PartialOrd, Eq, PartialEq, Clone, Debug)]
pub struct DockerImage {
    pub name: String,
    pub registry: String,
    pub tag: String,
}
